﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LimbManipulator : MonoBehaviour
{
    private Vector3 mOffset;
    private float mZCoord;

    [SerializeField] private LimbController m_limbController;
    [SerializeField] private SpriteRenderer m_circleSpriteRenderer;

    void OnMouseDown()
    {
        if (!m_limbController.m_isHandAttached)
        {
            mZCoord = Camera.main.WorldToScreenPoint(
            gameObject.transform.position).z;

            // Store offset = gameobject world pos - mouse world pos
            mOffset = gameObject.transform.position - GetMouseAsWorldPoint();

            m_limbController.m_isLimbDrugging = true;
            ChangeSpriteAlpha(0.3f);
        }
    }

    private void OnMouseUp()
    {
        StartCoroutine(DisableOnMouseDrag());
    }

    private IEnumerator DisableOnMouseDrag()
    {
        ChangeSpriteAlpha(1f);

        yield return new WaitForSeconds(0.1f);


        m_limbController.m_isLimbDrugging = false;
    }

    private Vector3 GetMouseAsWorldPoint()

    {
        // Pixel coordinates of mouse (x,y)

        Vector3 mousePoint = Input.mousePosition;


        // z coordinate of game object on screen

        mousePoint.z = mZCoord;


        // Convert it to world points

        return Camera.main.ScreenToWorldPoint(mousePoint);
    }


    void OnMouseDrag()
    {
        if (m_limbController.m_isHandAttached) return;

        transform.position = GetMouseAsWorldPoint() + mOffset;
    }

    public void ChangeSpriteAlpha(float alphaValue)
    {
        Color tmp = m_circleSpriteRenderer.color;
        tmp.a = alphaValue;
        m_circleSpriteRenderer.color = tmp;
    }
}