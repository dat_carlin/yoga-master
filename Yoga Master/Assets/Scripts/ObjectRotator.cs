﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectRotator : MonoBehaviour
{
    private float m_rotationSpeed = 1.5f;

    private void FixedUpdate()
    {
        transform.Rotate(0, m_rotationSpeed * Time.fixedDeltaTime, 0);

    }
}
