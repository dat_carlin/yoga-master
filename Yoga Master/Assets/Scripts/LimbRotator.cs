﻿using Obi;
using UnityEngine;

public class LimbRotator : MonoBehaviour
{
    [SerializeField] private Transform m_centralGameobject;
    [SerializeField] private ObiRope m_obiRope;

    public enum LimbType
    {
        None,
        Hand,
        Leg
    }

    [SerializeField] private LimbType m_limbtype;



    private void Update()
    {
        LookAtEndOfRope();
    }

    private void LookAtEndOfRope()
    {
        switch (m_limbtype)
        {
            case LimbType.Hand:
                {
                    var lookPos = m_centralGameobject.position - transform.position;
                    lookPos.x *= -1;
                    lookPos.y *= -1;
                    lookPos.z *= -1;
                    var rotation = Quaternion.LookRotation(lookPos);
                    transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * 10f);
                    break;
                }

            case LimbType.Leg:
                {
                    var lookPos = m_centralGameobject.position - transform.position;
                    lookPos.x *= -1;
                    lookPos.y *= -1;
                    lookPos.z *= -1;
                    var rotation = Quaternion.LookRotation(lookPos);
                    //transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * 10f);

                    transform.rotation = m_obiRope.GetParticleOrientation(1);
                    break;
                }
        }
      
    }
}