﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using UnityEngine.Serialization;

public class LimbController : MonoBehaviour
{
    [BoxGroup("References"), SerializeField]
    private Transform m_correctPosition;

    [BoxGroup("References"), SerializeField]
    private Transform m_limbObject;

    [BoxGroup("References"), SerializeField]
    private LimbManipulator m_manipulator;

    [HideInInspector] public bool m_isHandAttached;
    [HideInInspector] public bool m_isLimbDrugging;

    private readonly float m_correntZoneRange = 0.5f;


    private void Start()
    {
        CheckCorrectPositionPresence();
    }

    private void AttachHand()
    {
        m_isHandAttached = true;
    }

    private void DetachHand()
    {
        m_isHandAttached = false;
    }

    private void CheckCorrectPositionPresence()
    {
        if (m_correctPosition == null)
        {
            print("Забыл установить Correct Position для " + gameObject.name);
        }
    }

    private void LateUpdate()
    {
        CheckOnMouseUp();
    }

    private void CheckOnMouseUp()
    {
        if ((Input.GetMouseButtonUp(0)))
        {
            if ((m_isLimbDrugging) && (!m_isHandAttached))
            {
                if (Vector3.Distance(m_limbObject.position, m_correctPosition.position) < m_correntZoneRange)
                {
                    AttachHand();
                    LevelContoller.Singletone.IncreaseCorrentPoints();
                    m_manipulator.ChangeSpriteAlpha(0f);
                }
            }
        }
    }
}