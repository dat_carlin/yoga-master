﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelContoller : MonoBehaviour
{
    public static LevelContoller Singletone { get; private set; }

    [BoxGroup("UI"), SerializeField] private Scrollbar m_progressBar;
    [BoxGroup("UI"), SerializeField] private Button m_restartButton;
    [BoxGroup("UI"), SerializeField] private Button m_nextLevelButton;
    [BoxGroup("UI"), SerializeField] private Animator m_winnerPanelAnimator;
    [BoxGroup("UI"), SerializeField] private Text m_currentLevelAmount;
    [BoxGroup("UI"), SerializeField] private Text m_nextLevelAmount;
    [BoxGroup("UI"), SerializeField] private ParticleSystem[] m_finishParticles;
    [BoxGroup("UI"), SerializeField] private GameObject[] m_uselessUI;

    private int m_currentCorrectPoints;
    private int m_maxCorrectPoints = 4;
    private float m_progressBarStepValue;

    private bool m_canUpdateProgressBar;

    private void Awake()
    {
        Singletone = this;
        m_restartButton.onClick.AddListener(OpenNextScene);
        m_nextLevelButton.onClick.AddListener(OpenNextScene);
    }

    private void Start()
    {
        RefreshAndShowLevelAmount();
        CalculateProgressBarStep();
    }

    private void Update()
    {
        RefreshProgressBar();
    }

    private void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    private void IncreaseLevelAmount()
    {
        if (PlayerPrefs.HasKey("CurrentLevelAmount"))
        {
            PlayerPrefs.SetInt("CurrentLevelAmount", (PlayerPrefs.GetInt("CurrentLevelAmount") + 1));
        }
        else
        {
            PlayerPrefs.SetInt("CurrentLevelAmount", 1);
        }
    }

    private void RefreshAndShowLevelAmount()
    {
        if (PlayerPrefs.HasKey("CurrentLevelAmount"))
        {
            m_currentLevelAmount.text = PlayerPrefs.GetInt("CurrentLevelAmount").ToString();
            m_nextLevelAmount.text = (PlayerPrefs.GetInt("CurrentLevelAmount") + 1).ToString();
        }
        else
        {
            m_currentLevelAmount.text = 1.ToString();
            m_nextLevelAmount.text = 2.ToString();
        }
    }

    public void IncreaseCorrentPoints()
    {
        StartCoroutine(AllowRefreshingProgress());
    }

    private IEnumerator PlayWinnerPanelAnimator()
    {
        yield return new WaitForSeconds(2f);
        m_winnerPanelAnimator.Play("WinnerPanel");
    }

    private void CalculateProgressBarStep() => m_progressBarStepValue = 1f / m_maxCorrectPoints;

    private void RefreshProgressBar()
    {
        if (m_canUpdateProgressBar)
        {
            m_progressBar.size = Mathf.MoveTowards(m_progressBar.size, m_currentCorrectPoints * m_progressBarStepValue,
                Time.deltaTime);
        }
    }

    private IEnumerator AllowRefreshingProgress()
    {
        m_currentCorrectPoints++;
        m_canUpdateProgressBar = true;
        if (m_currentCorrectPoints >= m_maxCorrectPoints)
        {
            FixateWinGame();
        }

        yield return new WaitForSeconds(0.75f);
        m_canUpdateProgressBar = false;
    }

    private void FixateWinGame()
    {
        StartCoroutine(PlayWinnerPanelAnimator());
        IncreaseLevelAmount();
        PlayFinishParticles();
        HideUselessUI();
    }

    private void HideUselessUI()
    {
        for (int i = 0; i < m_uselessUI.Length; i++)
        {
            m_uselessUI[i].SetActive(false);
        }
    }

    private void PlayFinishParticles()
    {
        foreach (var t in m_finishParticles)
        {
            t.Play();
        }
    }

    private void OpenNextScene()
    {
        if (SceneManager.GetActiveScene().buildIndex < SceneManager.sceneCountInBuildSettings - 1)
        {
            print((SceneManager.GetActiveScene().buildIndex + 1));
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
        else
        {
            SceneManager.LoadScene(0);
        }
    }
}